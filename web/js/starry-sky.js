function createStar() {
    const star = document.createElement('div');
    star.className = 'star';
    star.style.left = `${Math.random() * window.innerWidth}px`;
    star.style.top = `${Math.random() * window.innerHeight}px`;
    document.getElementById('star-container').appendChild(star);
    // Make the star blink
    setInterval(() => {
        star.style.opacity = star.style.opacity === '0' ? '1' : '0';
    }, Math.random() * 1000 + 500); // Random interval for a more natural effect
}

// Create 100 stars. Adjust this number for more or fewer stars.
for (let i = 0; i < 100; i++) {
    createStar();
}
 
